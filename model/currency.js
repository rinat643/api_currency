'use strict';
const conf = require('../config/database.js');

const Schema = conf.Schema;
const mongoose = conf.mongoose;

// Валюты
const Currency = new Schema({
    name: { type : String , unique : true, required : true, dropDups: true},
    course_to: [{
        name: String,
        course: Number,
        date: { type: Date, default: Date.now},
    }],
});

// Логи
const Loggers = new Schema({
    date: { type: Date, default: Date.now },
    type: String,
    url: String
});


const CurrencyModel = mongoose.model('Currency', Currency);
const LoggersModel = mongoose.model('Loggers', Loggers);

module.exports.LoggersModel = LoggersModel;
module.exports.CurrencyModel = CurrencyModel;