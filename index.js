'use strict';
const cluster = require('cluster');
const {UpdateCurrency} = require('./tasks/tasks');
if (cluster.isMaster) {
    // Count the machine's CPUs
    // var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < 1; i += 1) {
        cluster.fork();
    }

} else {
    const conf = require('./config/conf');
    new UpdateCurrency();
    conf.app.use('/api', require('./api/index.js'));
    module.exports = conf.app;
}