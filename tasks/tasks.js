'use strict'
const fetch = require('node-fetch');
const {CurrencyModel} = require('../model/currency');

class UpdateCurrency {
    constructor(){
        CurrencyModel.find().exec((err, res)=>{
            if (err) throw err;
            else {
                if (res.length == 0) {
                    new Promise(async (resolve, reject)=>{
                        await this.update();
                        resolve()
                    }).then(()=>{
                        console.log("update")
                    }).catch((err)=>{
                        throw new Error(err);
                    })
                }
                console.log(`В базе ${res.length} курс валют`);
            }
        })
    }

    async reread(data, key){
        let result = [];
        for (let i of Object.keys(data)) {
            if (key == "USD"){
                result.push({
                    name: i,
                    course: data[i],
                    date: new Date()
                });
            } else if (i !== key) {
                result.push({
                    name: i,
                    course: data[key] / data[i],
                    date: new Date()
                });
            }
        }
        return result
    }

    async update() {
        const response = await fetch(`https://api.currencyfreaks.com/latest?apikey=${process.env.TOKEN_FREAK}`);
        const data = await response.json();
        // await CurrencyModel.find().remove();
        if (!data.base) throw new Error("Not requests in API !");
        new Promise(async (resolve, reject)=>{

            let dt = [{
                name: data.base,
                course_to: await this.reread(data.rates, 'USD')
            }];
            for (let i of Object.keys(data.rates)) {
                dt.push({
                    name: i,
                    course_to: await this.reread(data.rates, i)
                });
            }
            resolve(dt);
        }).then(async (dt)=>{
            await CurrencyModel.insertMany(dt)
            console.log("----Количество валют в справочнике-------")
            console.log((await CurrencyModel.find()).length)
        })

    }
}

const Currency = new UpdateCurrency();

setInterval(() => {
    let time = new Date().getHours();
    console.log("tasks start");
    if (time == 12) Currency.update();
}, 60000)

module.exports.UpdateCurrency = UpdateCurrency;