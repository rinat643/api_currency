'use strict';
// Просто настройка базы
const mongoose = require('mongoose');
let mongoDB = undefined;
if (process.env.MONGODB_USERNAME) {
    // ${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@
    mongoDB = `mongodb://localhost:27017/${process.env.MONGODB_DATABASE}`;
} else {
    throw "Not env settings"
}
const Schema = mongoose.Schema;

mongoose.connect(mongoDB, {useNewUrlParser: true});

mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', function (err) {
    console.error('connection error:', err.message);
});

db.once('open', function callback () {
    console.info("Connected to DB!");
});

module.exports.Schema = Schema;
module.exports.mongoose = mongoose;