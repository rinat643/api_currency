'use strict';
const fs = require('fs');
//Сам экспресс
const express = require('express');
// Парсинг куков
const cookieParser = require('cookie-parser');
// Сжатие всех данных для более быстрой работы
const compression = require('compression');
const logger = require('morgan');
const helmet = require('helmet');
const app = express();
const cors = require('cors');

const {configure, getLogger} = require('log4js');
configure({
  appenders: {
    everything: { type: 'file', filename: './mysite.log' }
  },
  categories: {
    default: { appenders: [ 'everything' ], level: 'debug' }
  }
});
const loggers = getLogger();
logger.level = 'debug';

app.set('TokenSecret', 'SecretKey');
app.use(cors());

const scriptSources = ["'self'", "'unsafe-inline'", "'unsafe-eval'", "ajax.googleapis.com"];
const connectSources = ["'self'", "*"];
// Основа
const router = new express.Router();

app.use(helmet());
app.use(helmet.frameguard());
app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ["'self'", '127.0.0.1'],
        scriptSrc: scriptSources,
        connectSrc: connectSources,
    }
}));

app.use(compression());
app.use(cookieParser());
// //Парсер страниц в читаемый вид
const bodyParser = require('body-parser');
// // Логи
router.use(logger('dev'));
// // Библиотечка для парсинга Json
router.use(bodyParser.json());
// // библиотечка для парсинга форм
router.use(bodyParser.urlencoded({ extended: false })); // Parse forms

// app.use(auth_proplast());

app.set('trust proxy', 1) // trust first proxy
fs.mkdir(`./public/media/temp/`,function(err,dir){
    if (err) {
        return null
    }
});
let servers = (app) => {
    return require('http').Server(app);
};

let server = servers(app);
server.listen(process.env.PORT_SERVICE);
module.exports.app = app;
module.exports.secret = {
    'secret': 'zpk4o28fcyd=zo!zrapsv4$bm#!r&wudr0=6zeg6g2$a=_31w9'
};
module.exports.settings = {
    'protocol': process.env.PROTOCOL||"http"
};
module.exports.router = router;
module.exports.TEST = process.env.TEST|false;
module.exports.log = loggers;