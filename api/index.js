const express = require('express');
const {LoggersModel, CurrencyModel} = require('../model/currency');
const {router} =  require('../config/conf.js');
const {check, validationResult} = require('express-validator/check');

async function midd(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept", "XMLHttpRequest","text/plain;charset=utf-8", "no-cache", "must-revalidate");
  if (req.query.token) {
    let type = "Иное";
    if (!req.query.date && req.query.course) type = "По паре";
    else if (req.query.date && !req.query.course) type = "По дате";
    let log = new LoggersModel({ 
      type: type, 
      url: req._parsedUrl.pathname
    });
    await log.save()
    return next();
  }
  res.json({
    error: "Not token"
  })
}


router.use(midd);

router.get('/get/logger/', async function (req, res) {
  let logger = await LoggersModel.find().limit(50);
  res.json(logger);
});

/* 
  @currency - Валюта главная
  @currency_to - С какой валютой сравнить 
  @date - Дата (пример 2021-02-24)
*/
router.get('/get/course/', 
  [
    check('currency').isString(),
    check('currency_to').isString(),
    check('date', new Date(`${new Date().toDateString()}`)).optional().isDate(),
  ],
  
  async function (req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let filters = {base: {}, course_to: {}};
    if (req.query.currency) filters.base['name'] = req.query.currency;
    if (req.query.currency_to) filters.course_to['name'] = req.query.currency_to;
    console.log(req.query)
    let course = await CurrencyModel.find({
      ...filters.base
    }).select({
      course_to: {
        $elemMatch: {
          ...filters.course_to,
          date: {
            $gte: req.query.date
          }
        }
      }
    });
    res.json(course);
});

router.get('/', function (req, res) {
    res.json({test: true});
});

module.exports = router;